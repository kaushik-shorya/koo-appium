package com.qa.pages;

import java.sql.Driver; 

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.service.DriverCommandExecutor;
import org.openqa.selenium.support.FindBy;

import com.qa.utils.TestUtils;

public class Search extends BasePage{
	TestUtils utils = new TestUtils();
	
	@FindBy(id = "com.koo.app:id/layout_search")
	public WebElement SearchButton;
	
	@FindBy(id = "com.koo.app:id/action_bar_et_search")
	public WebElement SearchField;
	
	@FindBy(xpath = "//android.widget.ImageButton")
	public WebElement BackButton;
	
	@FindBy(id = "com.koo.app:id/voice_search_image_view")
	public WebElement MicButton;
	
	@FindBy(xpath = "//android.widget.LinearLayout[@index='0']")
	public WebElement PeopleButton;
	
	@FindBy(xpath = "//android.widget.LinearLayout[@index='1']")
	public WebElement HashtagButton;
	
	@FindBy(xpath = "//android.widget.LinearLayout[@index='2']")
	public WebElement KooButton;
	
	@FindBy(id = "com.koo.app:id/toolbar_profile")
	public WebElement HomeScreen;
	
	@FindBy(id = "com.koo.app:id/search_view_pager")
	public WebElement SearchPage;
	
	@FindBy(xpath = "//android.widget.RelativeLayout[@index='0']")
	public WebElement PeopleSearched1;
	
	@FindBy(xpath = "//android.widget.RelativeLayout[@index='1']")
	public WebElement PeopleSearched2;
	
	//@FindBy(xpath = "//android.widget.RelativeLayout[@index='2']")
	//public WebElement PeopleSearched3;
	
	//@FindBy(xpath = "//android.widget.RelativeLayout[@index='3']")
	//public WebElement PeopleSearched4;
	
	@FindBy(id = "com.koo.app:id/bt_continue")
	public WebElement SkipButton;
	
	@FindBy(xpath = "//android.webkit.WebView")
	public WebElement UpdateNotification;
	
	@FindBy(xpath = "//android.widget.ImageView[@index='1']")
	public WebElement CloseButton;
	
	

	public Search(){
	}
	
	public void waitforHomeScreen() {
		waitForVisibility(HomeScreen);		
	}
	
	public void user_Tap_On_Search_Icon_And_Navigates_To_Search_Screen() throws InterruptedException {
		Thread.sleep(8000);
		//if(isElementPresent(CloseButton));{
			//click(CloseButton);
		//}
		click(SearchButton);
		Assert.assertTrue(isElementPresent(SearchPage));

		
	}
	
	 public void clickOnAppBackButton() throws InterruptedException  {
		 waitForVisibility(BackButton);
	     click(BackButton);
	     waitForVisibility(HomeScreen);
	     Assert.assertTrue(isElementPresent(HomeScreen));
	}
	
	
	public void VerifySearchFieldAndSearchResult(String searchValue) throws InterruptedException {
		user_Tap_On_Search_Icon_And_Navigates_To_Search_Screen();
		click(SearchField);
		SearchField.clear(); 
		SearchField.sendKeys(searchValue);
		clickAndroidDeviceBackBttn();
		Assert.assertTrue(isElementPresent(PeopleSearched1));
		Assert.assertTrue(isElementPresent(PeopleSearched2));

		
	}


	
	public void AndroidDeviceBackBttn() throws InterruptedException{
		user_Tap_On_Search_Icon_And_Navigates_To_Search_Screen();
		waitForVisibility(SearchField);
		clickAndroidDeviceBackBttn();
		clickAndroidDeviceBackBttn();
		Assert.assertTrue(isElementPresent(HomeScreen));
		
		
		
	}

	public void iVerifyFirstNameAndVerifyResult(String FirstName)  throws InterruptedException{
		//user_Tap_On_Search_Icon_And_Navigates_To_Search_Screen();
		waitForVisibility(SearchField);
		click(SearchField);
		SearchField.clear(); 
		SearchField.sendKeys(FirstName);
		clickAndroidDeviceBackBttn();
		Assert.assertTrue(isElementPresent(PeopleSearched1));
		Assert.assertTrue(isElementPresent(PeopleSearched2));
		
		
		
		
	}

	public void iverifyUserIsRegistered() {
		click(SkipButton);
		
	
		
	}

	public void iverifyLastNameAndVerifyRsult(String LastName) {
		waitForVisibility(SearchField);
		click(SearchField);
		SearchField.clear(); 
		SearchField.sendKeys(LastName);
		clickAndroidDeviceBackBttn();
		Assert.assertTrue(isElementPresent(PeopleSearched1));
		Assert.assertTrue(isElementPresent(PeopleSearched2));
		
	}

	public void iverifyHandleNameAndVerifyRsult(String HandleName) {
		waitForVisibility(SearchField);
		click(SearchField);
		SearchField.clear(); 
		SearchField.sendKeys(HandleName);
		clickAndroidDeviceBackBttn();
		Assert.assertTrue(isElementPresent(PeopleSearched1));
		Assert.assertTrue(isElementPresent(PeopleSearched2));
		
	}

	public void iverifywithAttheRateHandleNameAndVerifyRsult(String HandleName1) {
		waitForVisibility(SearchField);
		click(SearchField);
		SearchField.clear(); 
		SearchField.sendKeys(HandleName1);
		clickAndroidDeviceBackBttn();
		Assert.assertTrue(isElementPresent(PeopleSearched1));
		Assert.assertTrue(isElementPresent(PeopleSearched2));
		
		
	}

	public void iVerifyUserandHandleNameWithUPPERCASEAndlowercase(String searchValue1,String searchValue2,String HandleName2 ,String HandleName3) throws InterruptedException {
		Thread.sleep(6000);
		waitForVisibility(SearchField);
		click(SearchField);
		SearchField.clear(); 
		SearchField.sendKeys(searchValue1);
		clickAndroidDeviceBackBttn();
		Assert.assertTrue(isElementPresent(PeopleSearched1));
		Thread.sleep(6000);
		SearchField.clear(); 
		SearchField.sendKeys(searchValue2);
		clickAndroidDeviceBackBttn();
		//Assert.assertTrue(isElementPresent(PeopleSearched1));
		//Thread.sleep(6000);
		//SearchField.clear(); 
		//SearchField.sendKeys(HandleName2);
		//clickAndroidDeviceBackBttn();
		//Assert.assertTrue(isElementPresent(PeopleSearched1));
		//Thread.sleep(6000);
		//SearchField.clear(); 
		//SearchField.sendKeys(HandleName3);
		//clickAndroidDeviceBackBttn();
		//Assert.assertTrue(isElementPresent(PeopleSearched1));
		
		
	}

	public void iverifybyClearingAndWriteAgain(String searchValue) throws InterruptedException {
		click(SearchField);
		SearchField.clear(); 
		SearchField.sendKeys(searchValue);
		clickAndroidDeviceBackBttn();
		Assert.assertTrue(isElementPresent(PeopleSearched1));
		Thread.sleep(6000);
		click(SearchField);
		SearchField.clear();
		Thread.sleep(6000);
		click(SearchField);
		SearchField.clear(); 
		SearchField.sendKeys(searchValue);
		clickAndroidDeviceBackBttn();
		Assert.assertTrue(isElementPresent(PeopleSearched1));
	    
		
	}

	public void iVerifySearchedProfileandCameBack() throws InterruptedException {
		click(PeopleSearched1);
		
		
	}
	


	
			
				
	}
	 


	

	