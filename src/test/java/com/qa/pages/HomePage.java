package com.qa.pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {
	
	@FindBy(id= "com.koo.app:id/img_my_profile")
	public WebElement profilePicLocator;
	@FindBy(id= "com.koo.app:id/koo_imageview")
	public WebElement kooIcon;
	@FindBy(id="com.koo.app:id/blip_trending")
	public WebElement trendingKooIcon;
	@FindBy(xpath="//android.widget.TextView[@text='Trending Koos (24 hours)']")
	public WebElement trendingKoosText;
	@FindBy(xpath = "//android.Widget.ImageButton[@index='0']")
	public WebElement backButtonNavigation;
	@FindBy(xpath= "//android.widget.TextView[@text='Feed']")
	public WebElement feedTab;
	@FindBy(xpath="//android.widget.TextView[@text='People']")
	public WebElement peopleTab;
	@FindBy(xpath="//android.widget.TextView[@text='Trending']")
	public WebElement trendingTab;
	@FindBy(xpath="//android.widget.TextView[@text='Videos']")
	public WebElement videosTab;
	@FindBy(xpath="//android.widget.TextView[@text='COVID']")
	public WebElement  covidTab;
	@FindBy(xpath="//android.widget.TextView[@text='Following']")
	public WebElement followingTab;
	@FindBy(xpath="//android.widget.TextView[@text='New']")
	public WebElement newTab;
	@FindBy(xpath="//android.widget.TextView[@text='Polls']")
	public WebElement pollsTab;
	@FindBy(xpath="//android.widget.TextView[@text='TNPL']")
	public WebElement tnplTab;
	@FindBy(id= "com.koo.app:id/top_user_title")
	public WebElement peopleYouCanFollowText;
	@FindBy(id = "com.koo.app:id/layout_home")
	public WebElement homeExplorer;
	@FindBy(id = "com.koo.app:id/layout_explore")
	public WebElement hashTags;
	@FindBy(id = "com.koo.app:id/layout_search")
	public WebElement searchButton;
	@FindBy(id = "com.koo.app:id/layout_messages")
	public WebElement chatButton;
	@FindBy(id = "com.koo.app:id/layout_notification")
	public WebElement notificationButton;
	@FindBy(id = "com.koo.app:id/channel_name_textview")
	public WebElement kooCard;
	@FindBy(id = "com.koo.app:id/topic_title_textview")
	public WebElement kooFeedMessage;
	@FindBy(id = "com.koo.app:id/circular_fab")
	public WebElement kooCreationButton;
	@FindBy(id="com.koo.app:id/tv_button_text")
	public WebElement findFriendsOnKoo;
	
//	 Verify Navigations on Koo app
	@FindBy(id="com.koo.app:id/img_my_profile")
	public WebElement profilePic;
	@FindBy(id="com.koo.app:id/tv_edit_profile")
	public WebElement editProfileButtonElement;
	@FindBy(id="com.koo.app:id/layout_close")
	public WebElement backButtonProfilePage;
	@FindBy(xpath="//android.widget.TextView[@text='Messages']")
	public WebElement messageText;
	@FindBy(id="com.koo.app:id/top_user_title")
	public WebElement discoverRelevantPeopleText;
	
	
	public void verifyTheProfilePicture() {
		new BasePage()
			.isElementPresent(profilePicLocator);
	}

	public void verifyTheKooIcon() {
		new BasePage()
			.isElementPresent(kooIcon);
	}

	public void verifyTheTrendingKooIcon() {
		new BasePage()
			.isElementPresent(trendingKooIcon);
	}

	public void verifyFeed(String text) {
		Assert.
			assertTrue(feedTab.getText().equalsIgnoreCase(text));
	}
	
	public void verifyPeopleText(String text) {
		Assert.
			assertTrue(peopleTab.getText().equalsIgnoreCase(text));
	}
	
	public void verifyTrendingText(String text) {
		Assert.
			assertTrue(trendingTab.getText().equalsIgnoreCase(text));
	}
	
	public void verifyVideosText(String text) {
		Assert.
			assertTrue(videosTab.getText().equalsIgnoreCase(text));
	}
	public void verifyCOVIDText(String text) {
		Assert.
			assertTrue(covidTab.getText().equalsIgnoreCase(text));
	}
	public void verifyFollowingText(String text) {
		Assert.
			assertTrue(followingTab.getText().equalsIgnoreCase(text));
	}
	public void verifyNewText(String text) {
		Assert.
			assertTrue(newTab.getText().equalsIgnoreCase(text));
	}
	public void verifyPollsText(String text) {
		Assert.
			assertTrue(pollsTab.getText().equalsIgnoreCase(text));
	}
	
	public void verifyTNPLText(String text) {
		Assert.
			assertTrue(tnplTab.getText().equalsIgnoreCase(text));
	}
	
	public void verifyTheHomeExplorer() throws InterruptedException {
		Thread.sleep(5000);
		new BasePage().isElementPresent(homeExplorer);
		Thread.sleep(5000);
		new BasePage().isElementPresent(hashTags);
		Thread.sleep(5000);
		new BasePage().isElementPresent(searchButton);
		Thread.sleep(5000);
		new BasePage().isElementPresent(chatButton);
		Thread.sleep(5000);
		new BasePage().isElementPresent(notificationButton);
	}

	public void verifyTheKooCards() {
		new BasePage().
			isElementPresent(kooCard);
	}

	public void verifyOnScrollingFeedSeePeopleYouCanFollow(String text) {
		Assert.
			assertTrue(peopleYouCanFollowText.getText().
					equalsIgnoreCase(text));
	}

	public void verifyFeedMessage() {
		new BasePage().
			isElementPresent(kooFeedMessage);
	}

	public void verifyKooCreationButton() {
		new BasePage().
			isElementPresent(kooCreationButton);
	}

	public void verifyAudioPrimer() {

	}

	public void verifyUntilAudioPrimerCompletes() {

	}

	public void verifyTrendingHashTags() {
		new BasePage().click(hashTags);

	}

	public void verifyHashTagsList() {

	}

	public void verifyOnScrollingScreenUser() {

	}

	public void verifyScrollingShowsFeedScreen() {

	}

	public void verifyAllTabsPresentOnHomeScreen() {

	}
	
	public void verifyFindFriendsIsPresentAfterScrolling() throws Exception {
		new BasePage().scrollToElement(findFriendsOnKoo, "Down");
		new BasePage().isElementPresent(findFriendsOnKoo);
	}

//	 Validate the New User Flow
//
//	public void verifyNewUserLand() {
//		
//			
//
//	}
//
//	public void verifyOnScrollingDownShowsPeopleYouCanFollowList() {
//
//	}
//
//	public void verifyChannelCreationPopUp() {
//		new LoginPage().waitForLoginScreen();
//	}
//	
//
//	public void verifyOnFollowinAnyUser() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnBulkUserShouldSeeAMessage() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyYouWillSeeFeedFullOfTheirKoos() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTheFeedStructureInTheFollowingOrder() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyNewUserIsGettingAutoFollow() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyBelowProfessionalBulkFollow() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnBulkFollowUser() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTheFeedIsFullOFKoos() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTheFeedStructre() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTheNewUserIsGettingAutoFollow() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyBelowProfessionalBulk() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTheFeedStructure() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTrendingHastagsList() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTrendingHashtagsListInFeed() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTappingOnProfileNavigatestoProfileScreen() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnTappingKooIconNavigatesToTheFeed() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnTappingOnKooIconNavigateToTrendingKooScreen() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnTappingHomeIconUserIsNavigatedToTheTopOfTheFeed() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnTappingOnTheExploreButton() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnTappingOnTheSearchButton() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnTappingOnTheChatButton() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnTappingOnThePeopleUserIsNavigatedToPeopleScreen() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnTappingOnNewUsersButtonUserShouldNavigateToNewKoos() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyOnTappingOnKooCartUserNavigateToKooDetailView() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTappingOnTabForOneDayOldUser() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTappingOnOlympicsTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTappingOnCovidTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTappingOnFollowingTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTappingOnNewTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTappingOnPollsTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTappingOnVideosTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyTappingOnTNPLTabOneDayOldUsers() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyJustificationTexstAndComment() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	public void verifyKoosShouldDisplayAsPerTheTimeStamps() {
//		new LoginPage().waitForLoginScreen();
//	}
	
//	 Verify Navigations on Koo app
	public void verifyProfilePictureClick() throws InterruptedException {
		new BasePage().click(profilePic);
		Thread.sleep(5000);
		new BasePage().isElementPresent(editProfileButtonElement);
		Thread.sleep(5000);
		new BasePage().click(backButtonProfilePage);
		Thread.sleep(5000);
		new BasePage().isElementPresent(profilePic);
	}
	
	public void verifyOnClickRefreshOfKooIcon() {
		new BasePage().
			click(kooIcon);
		
	}
	
	public void verifyOnClickKooIconUserNavigatedToKooScreen() throws Exception {
		
			if(new BasePage().isElementPresent(trendingKooIcon))
			{
				Thread.sleep(5000);
				new BasePage().click(trendingKooIcon);
			
				if(new BasePage().isElementPresent(trendingKoosText))
				{
					Thread.sleep(5000);
					new BasePage().click(backButtonNavigation);
				}
			}
			else {
				throw new Exception("Element" + trendingKooIcon + "not found");
			}
	}
	
	public void verifyOnTappingToHomeIconUserNavigatedToTopOfFeed() {
		new BasePage().click(homeExplorer);
	}
	
	public void verifyOnSearchButtonUserNavigateToSearchScreen() throws Exception {
		if(new BasePage().isElementPresent(searchButton))
		{
			Thread.sleep(5000);
			new BasePage().click(searchButton);
		
			if(new BasePage().isElementPresent(backButtonNavigation))
			{
				Thread.sleep(5000);
				new BasePage().click(backButtonNavigation);
			}
			else {
				throw new Exception("Element" + backButtonNavigation + "not found");
			}
		}
		else {
			throw new Exception("Element" + searchButton + "not found");
		}
	}
	
	public void verifyOnTappingOnChatButtonIsNavigatedToChatScreenPressingBackButtonNavigatesToHomeScreen() throws Exception {
		if(new BasePage().isElementPresent(chatButton))
		{
			Thread.sleep(5000);
			new BasePage().click(chatButton);
		
			if(new BasePage().isElementPresent(messageText))
			{
				Thread.sleep(5000);
				new BasePage().click(backButtonNavigation);
			}
			else {
				throw new Exception("Element" + backButtonNavigation + "not found");
			}

		}
		else {
			throw new Exception("Element" + chatButton + "not found");
		}
	}

	public void verifyOnNotificationButtonNavigatesToNotificationScreenThenBackButtonNavigatesToHomeScreen() throws Exception {
		if(new BasePage().isElementPresent(notificationButton))
		{
			Thread.sleep(5000);
			new BasePage().click(notificationButton);
		
			if(new BasePage().isElementPresent(messageText))
			{
				Thread.sleep(5000);
				new BasePage().click(backButtonNavigation);
			}
			else {
				throw new Exception("Element" + backButtonNavigation + "not found");
			}
		}
		else {
			throw new Exception("Element" + chatButton + "not found");
		}
	}
	
	public void  verifyTapOnPeopleButtonNavigateUserToPeopleScreenPressingFeedNavigateToFeedScreen() throws Exception {
		if(new BasePage().isElementPresent(peopleTab))
		{
			Thread.sleep(5000);
			new BasePage().click(peopleTab);
		
			if(new BasePage().isElementPresent(discoverRelevantPeopleText))
			{
				Thread.sleep(5000);
				new BasePage().click(feedTab);
			}
			else {
				throw new Exception("Element" + discoverRelevantPeopleText + "not found");
			}
		}
		else {
			throw new Exception("Element" + peopleTab + "not found");
		}
	}
	
	public void verifyTapOnTrendingButtonNavigateToTrendingPageTapOnBackButtonNavigateToFeedScreen() throws Exception {
		if(new BasePage().isElementPresent(trendingTab))
		{
			Thread.sleep(5000);
			new BasePage().click(trendingTab);
		
			if(new BasePage().isElementPresent(discoverRelevantPeopleText))
			{
				Thread.sleep(5000);
				new BasePage().click(feedTab);
			}
			else {
				throw new Exception("Element" + discoverRelevantPeopleText + "not found");
			}
		}
		else {
			throw new Exception("Element" + trendingTab + "not found");
		}
	}
	
	public void peopleYouCanFollowText() {
		new BasePage().swipeToSeeDownsideElementsOfScreen(2);
		Assert.assertTrue(isElementPresent(peopleYouCanFollowText));
		
	}
	

}