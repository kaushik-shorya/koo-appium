package com.qa.stepdef;

import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class HomeScreenStepDef {

	@Given("^After login I am on the Homescreen$")
	public void verifyProfilePicture() {
		new HomePage().
			verifyTheProfilePicture();
	}

	@Then("^Verify the profile picture$")
	public void verifyTheProfilePicture() {
		new HomePage().verifyTheProfilePicture();
	}

	@Then("^Verify the koo icon at the top center$")
	public void verifyTheKooIcon() {
		new HomePage().verifyTheKooIcon();
	}

	@Then("^Verify the trending the koos icons on the top right screen$")
	public void verifyTheTrendingKooIcons() {
		new HomePage().verifyTheTrendingKooIcon();
	}

	@Then("^Verify the feed, people tab and new user tab for selected user in respective language.$")
	public void verifyFeedsPeople_etc() {
		new HomePage().verifyFeed("Feed");
		new HomePage().verifyPeopleText("People");
		new HomePage().verifyTrendingText("Trending");
		new HomePage().verifyVideosText("Videos");
		new HomePage().verifyCOVIDText("COVID");
		new HomePage().verifyFollowingText("Following");
		new HomePage().verifyNewText("New");
		new HomePage().verifyTNPLText("TNPL");
	}
	
	@Then("^Verify the home, explore, search, chat, and notification buttons at the bottom of the screen.$")
	public void verifyTheHomeExplore() throws InterruptedException {
		new HomePage().verifyTheHomeExplorer();
	}

	@Then("^Verify the koo cards of the followed users should be present in the feed.$")
	public void verifyTheKooCards() {
		new HomePage().verifyTheKooCards();
	}

	@Then("^Verifty on scrolling down in feed \"people you can follow\" list should be present$")
	public void verifyOnScrollingFeed() {
		new HomePage().verifyOnScrollingFeedSeePeopleYouCanFollow("People you can Follow");
	}

	@Then("^Verify at the end of feed Keep following users for seeing feed message should be displayed.$")
	public void verifyFeedMessage() {
		new HomePage().verifyFeedMessage();
	}

	@Then("^Verify Find your friends button and people you may know section-After giving contacts permission in feed and people tab.$")
	public void verifyFindYourFriends() throws Exception {
		new HomePage().verifyFindFriendsIsPresentAfterScrolling();
	}

	@Then("^Verify on tapping trending koos button user should navigate to respective screen.$")
	public void verifyOnTappingTrendingKoosButton() {
		new HomePage().verifyOnClickRefreshOfKooIcon();
	}



//	Validate the New User Flow

//	@Then("^Verify if new user lands on feed koo demo video should be present.$")
//	public void verifyNewUserLands() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on scrolling down in feed \"people you can follow\" list should be present$")
//	public void verifyOnScrollingDownShowsPeopleYouCanFollowList() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify channel creation popup should be displayed on tapping on like, rekoo, follow, posting comment & creating koo.$")
//	public void verifyChannelCreationPopUp() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on following any user the feed should automatically refresh and their koos should be displayed on feed.$")
//	public void verifyOnFollowinAnyUser() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^On bulk follow user should see a message on feed saying you have just followed top 30 creators.$")
//	public void verifyOnBulkUserShouldSeeAMessage() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^You will now see a feed full of their Koos. Please go to the People tab to discover more people and on refreshing the feed the message should disappear.$")
//	public void verifyYouWillSeeFeedFullOfTheirKoos() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify the feed structure in the following order for newly installed user.$")
//	public void verifyTheFeedStructureInTheFollowingOrder() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify new user is getting autofolllow (Apremeya, deepa, Sivanagarjuna etc).$")
//	public void verifyNewUserIsGettingAutoFollow() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify below professional bulk follow scenarios for new user on feed screen.$")
//	public void verifyBelowProfessionalBulkFollow() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^On bulk follow user should see a message on feed saying you have just followed top 30 creators.$")
//	public void verifyOnBulkFollowUser() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^You will now see a feed full of their Koos. Please go to the People tab to discover more people and on refreshing the feed the message should disappear.$")
//	public void verifyTheFeedIsFullOFKoos() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify the feed structure in the following order for newly installed user.$")
//	public void verifyTheFeedStructre() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify new user is getting autofolllow- Apremeya, deepa, Sivanagarjuna etc$")
//	public void verifyTheNewUserIsGettingAutoFollow() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify below professional bulk follow scenarios for new user on feed screen.$")
//	public void verifyBelowProfessionalBulk() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify the feed structure in the following order for newly installed user:$")
//	public void verifyTheFeedStructure() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify Trending hashtags list in Feed screen and it should not show for new user.$")
//	public void verifyTrendingHastagsList() {
//		
//	}
//
//	@Then("^Verify Trending hashtags list in Feed should start showing after following one user$")
//	public void verifyTrendingHashtagsListInFeed() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on tapping on the profile picture user is navigated to profile screen and on pressing back button user should navigate back to feed.$")
//	public void verifyTappingOnProfileNavigatestoProfileScreen() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^ Verify on tapping koo icon user is navigated to top of the feed and the feed should get refreshed.$")
//	public void verifyOnTappingKooIconNavigatesToTheFeed() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on tapping trending koos icon user should navigate to ternding koo screen and on tapping back button user should ba navigated back to feed$")
//	public void verifyOnTappingOnKooIconNavigateToTrendingKooScreen() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on tapping home icon user is navigated to top of the feed and the feed should get refreshed.$")
//	public void verifyOnTappingHomeIconUserIsNavigatedToTheTopOfTheFeed() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on tapping on the explore button user is navigated to explore screen and on pressing home button user should land back on feed.$")
//	public void verifyOnTappingOnTheExploreButton() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on tapping on the search button user is navigated to search screen and on pressing back button user should navigate back to feed.$")
//	public void verifyOnTappingOnTheSearchButton() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on tapping on the Chat button user is navigated to chat screen and on pressing back button user should navigate back to feed.$")
//	public void verifyOnTappingOnTheChatButton() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on tapping on the people user is navigated to people screen and on pressing feed button user should land back on feed.$")
//	public void verifyOnTappingOnThePeopleUserIsNavigatedToPeopleScreen() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on tapping on new users button user should navigate to new koos tab with new koos from recently joined users and on tapping on back button user should navigate back to feed.$")
//	public void verifyOnTappingOnNewUsersButtonUserShouldNavigateToNewKoos() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify on tapping on koo card(koo title) user should navigate to koo detail view and on pressing back button user should navigate back to feed.$")
//	public void verifyOnTappingOnKooCartUserNavigateToKooDetailView() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify tapping on Trending tab for one day old users , Trending tab should be displayed  and tapping back user should land on feed screen$")
//	public void verifyTappingOnTabForOneDayOldUser() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify tapping on olympics tab for one day old users , Olympics tab should be displayed  and tapping back user should land on feed screen$")
//	public void verifyTappingOnOlympicsTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify tapping on Covid tab for one day old users , Covid tab should be displayed  and tapping back user should land on feed screen$")
//	public void verifyTappingOnCovidTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify tapping on following tab for one day old users , following tab should be displayed  and tapping back user should land on feed screen$")
//	public void verifyTappingOnFollowingTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify tapping on new tab for one day old users , new tab should be displayed  and tapping back user should land on feed screen$")
//	public void verifyTappingOnNewTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify tapping on polls tab for one day old users , polls tab should be displayed  and tapping back user should land on feed screen$")
//	public void verifyTappingOnPollsTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify tapping on Videos tab for one day old users , Videos tab should be displayed  and tapping back user should land on feed screen$")
//	public void verifyTappingOnVideosTab() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify tapping on TNPL tab for one day old users , TNPL tab should be displayed  and tapping back user should land on feed screen$")
//	public void verifyTappingOnTNPLTabOneDayOldUsers() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^Verify justification text and comment threads should not be displayed in any of the other tabs execpt feeds tab$")
//	public void verifyJustificationTexstAndComment() {
//		new LoginPage().waitForLoginScreen();
//	}
//
//	@Then("^verify koos should be displayed as per the timestamps(latest koo first) in all of the home tabs$")
//	public void verifyKoosShouldDisplayAsPerTheTimeStamps() {
//		new LoginPage().waitForLoginScreen();
//	}

//	verify navigations of Koo App 
	@Then("^Verify on tapping on the profile picture user is navigated to profile screen and on pressing back button user should navigate back to feed.$")
    public void verifyProfiilePictureClick() throws InterruptedException {
       new HomePage().
       	verifyProfilePictureClick();
    }
	@Then("^Verify on tapping koo icon user is navigated to top of the feed and the feed should get refreshed.$")
	public void verifyRefreshOnKooIconClick() {
		new HomePage().
			verifyOnClickRefreshOfKooIcon();
	}
	
	@Then("^Verify on tapping trending koos icon user should navigate to ternding koo screen and on tapping back button user should ba navigated back to feed$")
	public void verifyTappingOnTrendingKoosIconUserNavigateToKooScreen() throws Exception {
		new HomePage().
			verifyOnClickKooIconUserNavigatedToKooScreen();
	}
	
	@Then("^Verify on tapping home icon user is navigated to top of the feed and the feed should get refreshed.$")
	public void verifyOnTappingHomeIconUserNavigatedToTopOfTheFeed() throws InterruptedException {
		new HomePage().
			verifyOnTappingToHomeIconUserNavigatedToTopOfFeed();
	}
	
	@Then("^Verify on tapping on the search button user is navigated to search screen and on pressing back button user should navigate back to feed.$")
	public void verifyOnSearchButtonClickNavigateToSearchScreenThenBackButtonToHomeScreen() {
		new HomePage().
			verifyOnTappingToHomeIconUserNavigatedToTopOfFeed();
	}
	
	@Then("^Verify on tapping on the Chat button user is navigated to chat screen and on pressing back button user should navigate back to feed.$")
	public void verifyOnTappingOnChatButtonIsNavigatedToChatScreenPressingBackButtonNavigatesToHomeScreen() throws Exception {
		new HomePage().
			verifyOnTappingOnChatButtonIsNavigatedToChatScreenPressingBackButtonNavigatesToHomeScreen();
	}
	
	@Then("^Verify on tapping on the notification button user is navigated to notification screen and on pressing back button user should navigate back to feed.$")
	public void verifyOnNotificationButtonNavigatesToNotificationScreenThenBackButtonNavigatesToHomeScreen() throws Exception {
		new HomePage().
		verifyOnNotificationButtonNavigatesToNotificationScreenThenBackButtonNavigatesToHomeScreen();
	}
	
	@Then("^Verify on tapping on the people user is navigated to people screen and on pressing feed button user should land back on feed.$")
	public void verifyTapOnPeopleButtonNavigateUserToPeopleScreenPressingFeedNavigateToFeedScreen() throws Exception {
		new HomePage().
			verifyTapOnPeopleButtonNavigateUserToPeopleScreenPressingFeedNavigateToFeedScreen();
	}
	
	@Then("^Verify tapping on Trending tab for one day old users , Trending tab should be displayed  and tapping back user should land on feed screen$")
	public void verifyTapOnTrendingButtonNavigateToTrendingPageTapOnBackButtonNavigateToFeedScreen() throws Exception {
		new HomePage().
			verifyTapOnTrendingButtonNavigateToTrendingPageTapOnBackButtonNavigateToFeedScreen();
	}
	
//	People you can follow
	
	@Then("^Verify the people you can follow list should be present after every few koo cards.$")
	public void peopleYouCanFollowText() {
		new HomePage().
			peopleYouCanFollowText();
	}
	
	
	
	
	
}
