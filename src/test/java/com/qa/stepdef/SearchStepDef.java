package com.qa.stepdef;

import static org.junit.Assert.assertArrayEquals; 
import static org.junit.Assert.assertEquals;
import org.junit.Assert;

import com.qa.pages.LoginPage;
import com.qa.pages.Search;
import io.cucumber.java.en.Then;


public class SearchStepDef {
	
	@Then("^I verify by tapping on Skip/Continue button.")
	public void iVerifyByTapOnSkip() throws InterruptedException{
		new Search().iverifyUserIsRegistered();
	}
	
	@Then("^I Verify by tap on Search icon user should navigate to search screen with VKB open and Cursor should be blinking in search field")
	public void user_Tap_On_Search_Icon_And_Navigates_To_Search_Screen() throws InterruptedException {
		//new Search().waitforHomeScreen();
		//new Search().clickOnSearchButton();
		new Search().user_Tap_On_Search_Icon_And_Navigates_To_Search_Screen();		
    }
	
	@Then("^I Verify by tap on App back button, VKB should close and user should navigate back to respective Home tab screen from where he came.")
	 public void clickOnAppBackButton() throws Exception {
        new Search().clickOnAppBackButton();
        //new Search().Home_Tab_Screen();
	
	}
	
	@Then("^I Verify by tap on Mobile back button, VKB should close and again tap on mobile back button user should navigate back to respective Home tab screen from where he came.")
	public void clickAndroidDeviceBackBttn() throws InterruptedException {
    	new Search().AndroidDeviceBackBttn();
    	//new Search().Home_Tab_Screen1();
   }
	
	@Then("^I Search for user \"([^\"]*)\" and verify search results.")
	public void iVerifySearchFieldAndSearchResult(String searchValue) throws InterruptedException {
		new Search().VerifySearchFieldAndSearchResult(searchValue);

  }

	@Then("^I Search user name with first \"([^\"]*)\" and verify results")
	public void iVerifyFirstNameAndVerifyResult(String FirstName) throws InterruptedException {
		new Search().iVerifyFirstNameAndVerifyResult(FirstName);
  }
	
   @Then("^I Search last name with first \"([^\"]*)\" and verify results")
   public void iVerifyLastNameAndVerifyResult(String LastName) throws InterruptedException {
	   new Search().iverifyLastNameAndVerifyRsult(LastName);
	   
   }
   

   @Then("^I Search handle name with first \"([^\"]*)\" and verify results")
   public void iVerifyHandleNameAndVerifyResult(String HandleName) throws InterruptedException {
	   new Search().iverifyHandleNameAndVerifyRsult(HandleName);
   }
   
   @Then("^I Search handle name with @ \"([^\"]*)\" and verify search results")
   public void iVerifywithAttheRateHandleNameAndVerifyResult(String HandleName1) throws InterruptedException {
	   new Search().iverifywithAttheRateHandleNameAndVerifyRsult(HandleName1);
	   
   }
   
   @Then("^I Search user name and handle name with Uppercase and Lowercase letters \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" and verify search results")
   public void iVerifyUserandHandleNameWithUPPERCASEAndlowercase(String searchValue1,String searchValue2,String HandleName2,String HandleName3 ) throws InterruptedException {
	   new Search().iVerifyUserandHandleNameWithUPPERCASEAndlowercase(searchValue1,searchValue2,HandleName2,HandleName3);
   }
   
   @Then("^I Search user name and clear the search results and \"([^\"]*)\" again search for the same username and handle.")
   public void iverifybyClearingAndWriteAgain(String searchValue) throws InterruptedException {
	   new Search().iverifybyClearingAndWriteAgain(searchValue);
   }
   
   @Then("^I Verify navigation to searched user profile, navigate back to search screen and verify search results be present as earlier.")
   public void iVerifySearchedProfileandCameBack() throws InterruptedException {
	   new Search().iVerifySearchedProfileandCameBack();
   }
}
 