package com.qa.stepdef;

import com.qa.pages.LanguageSelectionPage; 
import com.qa.pages.LoginPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class CommonStepsDef {

    @Then("^I select English Language as App language$")
    public void selectEngilshLanguage() throws InterruptedException {
    	new LanguageSelectionPage().waitForLanguageScreen();
    	new LanguageSelectionPage().clickOnEnglishLanguage();
    }

    @Then("^I am logged in into Koo Ap using country code \"([^\"]*)\" and Mobile number \"([^\"]*)\"$")
    public void logInUsingMobileNumber(String countrycode, String mobnumber) throws Exception {
    	new LoginPage().waitForLoginScreen();
    	new LoginPage().enterMobileNumber(countrycode, mobnumber);
    	new LoginPage().enterReceivedOTPFromMobile();
    }
    
}
