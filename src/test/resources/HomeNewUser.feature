Feature: Home Screen New User Scenarios.


	Background: Validate the registration process through email
	
	    Given User is on Language Selection Screen
	    When I selects English as Language
	    Then I Verify user navigate to Login screen
	    Then I Click on Sign In With Email option to login
	    Then I Enter Email id "<username>" and click on Proceed
	    Then I Receive Otp from "<email>", "<password>" and "<subject>"and enter it into OTP Field 
	    Then I Verify User is registered successfully
	    Examples:  
	      | username | email | password | subject|
	      |rajubugreporter| rajubugreporter@gmail.com| raju@updoer| Your Koo OTP|
	      
	      
	      
	 Scenario: Validate the New User flow
	    Given Verify if new user lands on feed koo demo video should be present.
	    Then Verify on scrolling down in feed "people you can follow" list should be present
	    Then Verify at the end of feed Keep following users for seeing feed message should be displayed.
	    Then Verify channel creation popup should be displayed on tapping on like, rekoo, follow, posting comment & creating koo.
	    Then Verify on following any user the feed should automatically refresh and their koos should be displayed on feed.
	    Then On bulk follow user should see a message on feed saying you have just followed top 30 creators.
	    Then You will now see a feed full of their Koos. Please go to the People tab to discover more people and on refreshing the feed the message should disappear.
	    Then Verify the feed structure in the following order for newly installed user.
	    Then Verify new user is getting autofolllow- Apremeya, deepa, Sivanagarjuna etc
	    Then Verifty on scrolling down in feed "people you can follow" list should be present
	    Then Verify at the end of feed Keep following users for seeing feed message should be displayed.
	    Then Verify channel creation popup should be displayed on tapping on like, rekoo, follow, posting comment & creating koo.
	    Then Verify on following any user the feed should automatically refresh and their koo's should be displayed on feed.
	    Then On bulk follow user should see a message on feed saying you have just followed top 30 creators.
	    Then You will now see a feed full of their Koos. Please go to the People tab to discover more people and on refreshing the feed the message should disappear.
	    Then Verify the feed structure in the following order for newly installed user.
	    Then Verify new user is getting autofolllow (Apremeya, deepa, Sivanagarjuna etc).
	    Then Verify below professional bulk follow scenarios for new user on feed screen.
	    Then Verify the feed structure in the following order for newly installed user:
	    Then Verify Trending hashtags list in Feed screen and it should not show for new user.
	    Then Verify Trending hashtags list in Feed should start showing after following one user	