#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Search Functional
  I want to use this template for my feature file
  
  Background:
    Given I select English Language as App language
    Then I am logged in into Koo Ap using country code "United States" and Mobile number "4153389516"

  @tag1
  Scenario Outline: Functional
  	#Given User is on Language Selection Screen
  	#When I selects English as Language
  	#Then I Verify user navigate to Login screen
  	#Then I am logged in into Koo Ap using country code "United States" and Mobile number "4153389516"
  	#Then I Verify User is registered successfully
  	Then I verify by tapping on Skip/Continue button.
    Then I Verify by tap on Search icon user should navigate to search screen with VKB open and Cursor should be blinking in search field
    Then I Verify by tap on App back button, VKB should close and user should navigate back to respective Home tab screen from where he came.
    Then I Verify by tap on Mobile back button, VKB should close and again tap on mobile back button user should navigate back to respective Home tab screen from where he came.
    Then I Search for user "<searchValue>" and verify search results.
    Then I Search user name with first "<FirstName>" and verify results
    Then I Search last name with first "<LastName>" and verify results
    Then I Search handle name with first "<HandleName>" and verify results
    Then I Search handle name with @ "<HandleName1>" and verify search results
    Then I Search user name and handle name with Uppercase and Lowercase letters "<searchValue1>" "<searchValue2>" "<HandleName2>" "<HandleName3>" and verify search results
    Then I Search user name and clear the search results and "<searchValue>" again search for the same username and handle.
    Then I Verify navigation to searched user profile, navigate back to search screen and verify search results be present as earlier.
    Examples: 
  	|searchValue| |FirstName| |LastName| |HandleName| |HandleName1| |HandleName2| |HandleName3| |searchValue1| |searchValue2|
  	|Sushil Kumar Modi| |Sushil| |Kumar Modi| |sushilmodi| |@sushilmodi| |@SUSHILMODI| |@sushilmodi| |SHUSHIL KUMAR MODI| |sushil kumar modi|
    
 