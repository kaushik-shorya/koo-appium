@test
Feature: Home Screen Scenarios

  Background: 
    Given I select English Language as App language
    Then I am logged in into Koo Ap using country code "United States" and Mobile number "4153389516"
    

  Scenario Outline: Validate the registration process through email
    Given User is on Language Selection Screen
    When I selects English as Language
    Then I Verify user navigate to Login screen
    Then I Click on Sign In With Email option to login
    Then I Enter Email id "<username>" and click on Proceed
    Then I Receive Otp from "<email>", "<password>" and "<subject>"and enter it into OTP Field
    Then I Verify User is registered successfully
    Examples:  
      | username | email | password | subject|
      |rajubugreporter| rajubugreporter@gmail.com| raju@updoer| Your Koo OTP|
      
   #Scenario Outline: Validate the registration process through Mobile Phone
#
    #Given User is on Language Selection Screen
    #When I selects English as Language
    #Then I Verify user navigate to Login screen
    #Then I Enter Mobile country Code "<CountryCode>" and Mobile number "<mobilenum>" and click on Proceed
    #Then I Receive Otp from Mobile and enter it
    #Then I Verify User is registered successfully
    #Examples:  
      #| CountryCode | mobilenum |
      #|United States| 4153389516|
      
      
    Scenario: Validate the Feed UI

    Examples: 
      | username        | email                     | password    | subject      |
      | rajubugreporter | rajubugreporter@gmail.com | raju@updoer | Your Koo OTP |

  Scenario: Validate the Feed UI
    Given After login I am on the Homescreen
    Then Verify the profile picture
    Then Verify the koo icon at the top center
    Then Verify the trending the koos icons on the top right screen
    Then Verify the feed, people tab and new user tab for selected user in respective language.
    Then Verify the home, explore, search, chat, and notification buttons at the bottom of the screen.
    Then Verify the koo cards of the followed users should be present in the feed.
    Then Verifty on scrolling down in feed "people you can follow" list should be present
    Then Verify at the end of feed Keep following users for seeing feed message should be displayed.
    Then Verify Find your friends button and people you may know section-After giving contacts permission in feed and people tab.
    Then Verify on tapping trending koos button user should navigate to respective screen.

  Scenario: Verify Navigations on Koo app
    Then Verify on tapping on the profile picture user is navigated to profile screen and on pressing back button user should navigate back to feed.
    Then Verify on tapping koo icon user is navigated to top of the feed and the feed should get refreshed.
    Then Verify on tapping trending koos icon user should navigate to ternding koo screen and on tapping back button user should ba navigated back to feed
    Then Verify on tapping home icon user is navigated to top of the feed and the feed should get refreshed.
    #Then Verify on tapping on the explore button user is navigated to explore screen and on pressing home button user should land back on feed.
    Then Verify on tapping on the search button user is navigated to search screen and on pressing back button user should navigate back to feed.
    Then Verify on tapping on the Chat button user is navigated to chat screen and on pressing back button user should navigate back to feed.
    Then Verify on tapping on the notification button user is navigated to notification screen and on pressing back button user should navigate back to feed.
    Then Verify on tapping on the people user is navigated to people screen and on pressing feed button user should land back on feed.
    #Then Verify on tapping on new users button user should navigate to new koos tab with new koos from recently joined users and on tapping on back button user should navigate back to feed.
    #Then Verify on tapping on koo card(koo title) user should navigate to koo detail view and on pressing back button user should navigate back to feed.
    Then Verify tapping on Trending tab for one day old users , Trending tab should be displayed  and tapping back user should land on feed screen

  Scenario: Verify Koo Car functionality
    Then Verify profile picture, name, handel, profession and time of koo creation should be displayed on every koo card.
    Then Verify for non followed user follow button should be displayed.
    Then Verify the created koo content should be present on the koo card.
    Then Verify the comment, rekoo, like, share buttons should be present below the koo content.
    Then Verify on tapping on profile picture, name or handel user should be navigated to the koo creators profile.
    Then Verify on tapping on the play button on audio or video koo card it should start to play.
    Then Verify on tapping on the image koo it should open in full screen.
    Then Verify on tapping on news link it should navigate to perticular news item.
    Then Verify on rekooed koo cards rekoo icon along with "Username rekkoed" should be displayed.
    Then Verify if koo contains any comments that comment should be displayed along with the parent koo and show thread button should be displayed."
    Then Verify on tapping on hash tags present in koo title user should navigate to koo present with perticular hashtags and
    Then Verify on tapping on back button user should navigate back to feed.
    Then Verify on tapping on mentions user should navigate to mentioned profileand on tapping on back button user should navigate back to feed
    Then Verify in comments screen parent koo creator profile pic should be displayed along with Replying to @user handel should be displayed and on dragging the screen down the parent koo should be displayed with profile picture, name and handel
    Then Verify on tapping on comment icon on koo card user should navigate to comments screen and on tapping on back button user should navigate back to feed."
    Then Verify on tapping on rekoo icon user should get a bottom bar with rekoo, rekoo with comment options should be displayed.
    Then Verify on tapptng on rekoo with comment, rekoo with comment screen should be displayed and on pressing back button user should navigate to feed screen."
    Then Verify on tapptng on share icon whats app send to screen should be displayed and on pressing back button user should navigate to feed screen."
    Then Verify on tapping on show threads user should navigate to koo detail view which contains all the comments to the koo
    Then Verify on tapping on rekoo or rekoo with comment form the botton bar the perticular koo should be rekooed and the rekoo icon should turn green andcount should increase by 1.
    Then Verify on tapping on like button the like button should turn to blue color and the count should increase by 1.
    Then Verify on posting a comment on koo comment count should increase.
    Then Verify user should be able to see comments bar once user has spent 3 seconds on koo.
    Then Verify on tapping comments bar user should navigate to koo creations screen where user can comment for respective koo
    Then Verify user is able to see the profile icon with yellow colour on koo card.
    Then Verify after posting the new koo share koo through facebook button should show below the posted koos with blink.
    Then Verify on tapping it should navigate to respective page and shared koo link should display.
    Then Verify posted poll should show on feed along with percentage and poll end date.
    Then Verify users vote for the created poll percentage should increase and voted count should show below the poll.
    Then Verify user should be able to vote only once for each poll.
    Then Verify on tapping poll title, poll options, votes section and poll duration user should navigate to koo detailed screen.
    Then Verify user is able to see below icons below polls, Comment, rekoo, like, facebook and whatsapp share icons
    Then Verify user is able comment and rekoo with comment for the polls using below options, Audio, Video, Hashtags, Mentions, News, youtube videos, images"
    Then Verify comment, rekoo and liked text on top of the polls
    Then Verify the static comment box on every koo card.
    Then Verify on tapping on comment box user should navigate to comment creation screen for the particular koo.
    Then Verify user is able to see GIF images on feed screen.
    Then Verify more like this button should be displayed when users have posted a koo with hashtags
    Then Verify tapping on more like this button users should land to that particular hashtag screen
    Then Tapping back button from hashtag screen users should land back to that particular card(from where the user tapped on morelikethis)
    Then Some users profiles images should be displayed next to morelike this button

# People you can follow

  Scenario: Verify People you can follow
    Given User is on Home Screen 
    Then Verify the people you can follow list should be present after every few koo cards.
		Then Verify view all button should be present beside the people you can follow text.
		Then Verify the list is scrollable and the scrolling should be smooth.
		Then Verify at the end of the list Discover grate minds card is displayed and tapping anywhere on that card user should be navigated to peoples screen.
		Then Verify each list contains different set of public profiles.
		Then Verify profile picture, name, profession and follow button should be displayed on every card.
		Then Verify on tapping any where on the card(except follow button) user is navigated to perticular public profile.
		Then Verify on tapping follow button on the public profile the profile should disappear from the list and the koos and rekoos of perticular user should be displayed on the feed.
		Then Verify on tapping on follow button user navigates to peoples screen.
		
		
	Scenario: Verify Report koo
		Then Verify report koo option is shown on every koo card except own koos.
		Then Verify user is able to report koo from feed, public profile, koo detail view screen.
		Then Verify on report a koo user should be asked for reason to report the koo.
		Then Verify the reported koo is shown in reported koo tab in dashboard.
		Then Verify on reporting a koo the perticular koo should not show to the reported user.   
		Then Verify on reporting a koo the perticular koo should not show to the reported user.

